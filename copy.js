rm -rf lib src
rsync -ar --progress ../merge-graphql-schemas/ ./ --exclude 'node_modules' --exclude '.git' --exclude '.gitignore'
rm -rf .git
git init
git add --all
git commit -n -m "build"

git checkout -B develop
git remote add origin git@bitbucket.org:Ephys/merge-graphql-schemas.git
git push --force origin develop
