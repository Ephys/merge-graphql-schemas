'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _fs = require('fs');

var _fs2 = _interopRequireDefault(_fs);

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const fileLoader = folderPath => {
  const dir = folderPath; // path.join(__dirname, folderPath);
  const files = [];
  _fs2.default.readdirSync(dir).forEach(f => {
    const ext = _path2.default.extname(f);
    const filesDir = _path2.default.join(dir, f);

    switch (ext) {
      case '.js':
        {
          const file = require(filesDir); // eslint-disable-line
          files.push(file.default);
          break;
        }

      case '.graphqls':
      case '.graphql':
        {
          const file = _fs2.default.readFileSync(filesDir);
          files.push(file.toString());
          break;
        }

      default:
    }
  });
  return files;
};

exports.default = fileLoader;