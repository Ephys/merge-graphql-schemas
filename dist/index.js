'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.mergeTypes = exports.mergeResolvers = exports.mergeGraphqlSchemas = undefined;

var _graphqlTools = require('graphql-tools');

var _file_loader = require('./file_loader');

var _file_loader2 = _interopRequireDefault(_file_loader);

var _merge_types = require('./merge_types');

var _merge_types2 = _interopRequireDefault(_merge_types);

var _merge_resolvers = require('./merge_resolvers');

var _merge_resolvers2 = _interopRequireDefault(_merge_resolvers);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const mergeGraphqlSchemas = (folderPath, debug = false) => {
  const typesArray = (0, _file_loader2.default)(`${folderPath}/types`);
  const resolversArray = (0, _file_loader2.default)(`${folderPath}/resolvers`);

  const typeDefs = (0, _merge_types2.default)(typesArray);
  const resolvers = (0, _merge_resolvers2.default)(resolversArray);

  if (debug === true) {
    console.log('===> SCHEMA: ', typeDefs); // eslint-disable-line
    console.log('===> RESOLVERS: ', resolvers); // eslint-disable-line
  }

  return (0, _graphqlTools.makeExecutableSchema)({ typeDefs, resolvers });
};

exports.mergeGraphqlSchemas = mergeGraphqlSchemas;
exports.mergeResolvers = _merge_resolvers2.default;
exports.mergeTypes = _merge_types2.default;