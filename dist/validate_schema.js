'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _graphqlTag = require('graphql-tag');

var _graphqlTag2 = _interopRequireDefault(_graphqlTag);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const validate = type => {
  try {
    _graphqlTag2.default`${type}`; // eslint-disable-line
  } catch (err) {
    if (process.env.NODE_ENV !== 'test') {
      console.log('===> Error in: ', type); // eslint-disable-line
    }
    throw err;
  }
};

exports.default = (schema, customTypes) => {
  customTypes.forEach(ct => validate(ct));
  validate(schema);
};